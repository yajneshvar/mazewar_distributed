import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Hashtable;
import java.util.concurrent.PriorityBlockingQueue;

public class PacketProducer implements Runnable {


   private PriorityBlockingQueue<MPacket> queue = null;
   private MSocket mSocket  =  null;



   public PacketProducer(PriorityBlockingQueue<MPacket> packet_queue, MSocket mSocket){
      this.queue = packet_queue;
      this.mSocket = mSocket;
   
   }

   public void run() {
      
        MPacket received = null;
        if(Debug.debug) System.out.println("Starting Packet Producer");
        while(true){
            try{
                received = (MPacket) mSocket.readObject();
                queue.put(received);
//                System.out.println("Received " + received);
            }catch(IOException e){
                e.printStackTrace();
            }catch(ClassNotFoundException e){
                e.printStackTrace();
            }
      }
   }
}
