import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Hashtable;
import java.util.concurrent.PriorityBlockingQueue;

public class PacketConsumer implements Runnable {


   private PriorityBlockingQueue<MPacket> queue = null;
   private Hashtable<String, Client> clientTable = null;
   private int seq_num;



   public PacketConsumer(PriorityBlockingQueue<MPacket> packet_queue,Hashtable<String,Client> clientTable){
      this.queue = packet_queue;
      this.clientTable = clientTable;
      this.seq_num = -1;
   
   }

   public MPacket get_packet(){
      MPacket peeked = null;

      do{
         peeked = queue.peek(); 
         if(peeked != null)System.out.println("Sequence Number is " + peeked.sequenceNumber);
      }while(peeked == null || ( seq_num != -1 &&  peeked.sequenceNumber != (seq_num + 1) )); 

      try{
         peeked = queue.take();
         seq_num = peeked.sequenceNumber;
      }catch(InterruptedException ex){
         ex.printStackTrace();
      }

         return peeked;
   }

   public void run() {
   
      
        MPacket received = null;
        MPacket peek_packet = null;
        Client client = null;
        if(Debug.debug) System.out.println("Starting Packet Consumer");
        while(true){
  //          try{
                //received = queue.take();
                received = get_packet();
                System.out.println("Received " + received);
                System.out.println("Yaj " + received.sequenceNumber);
                client = clientTable.get(received.name);
                if(received.event == MPacket.UP){
                    client.forward();
                }else if(received.event == MPacket.DOWN){
                    client.backup();
                }else if(received.event == MPacket.LEFT){
                    client.turnLeft();
                }else if(received.event == MPacket.RIGHT){
                    client.turnRight();
                }else if(received.event == MPacket.FIRE){
                    client.fire();
                }else if(received.event == MPacket.PROJECTILE){
                    client.projectile();
                }else{
                    throw new UnsupportedOperationException();
                }
           // }
            //catch(InterruptedException ex){
            //    ex.printStackTrace();
            //}

        }
   }
 
}
