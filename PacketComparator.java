import java.util.Comparator;

public class PacketComparator implements Comparator<MPacket> {

    @Override
    public int compare(MPacket packet1, MPacket packet2){

          if(packet1.sequenceNumber < packet2.sequenceNumber){
              return -1;
          }
          if(packet1.sequenceNumber > packet2.sequenceNumber){
              return 1;
          }
          return 0;
    }




}
